NAME=two-column-example

dockerimage:=registry.gitlab.com/mark.e.royer/texlive-full-with-emacs:20210611
dockeropts:=-u 1000:1000 -w /test \
-v $(CURDIR):/test \
-v /etc/group:/etc/group:ro \
-v /etc/passwd:/etc/passwd:ro \
-it --rm

.PHONY: clean startdocker startdocker-build warnings

$(NAME).pdf: $(NAME).tex $(NAME).bib
	latexmk -pdf  $(NAME).tex

warnings: $(NAME).pdf
	@grep Warning $(NAME).log;true

# Start docker and leave open
startdocker:
	docker run $(dockeropts) $(dockerimage)

# If you don't want to keep the container alive, build the default
# make target and exit.
startdocker-build:
	docker run $(dockeropts) $(dockerimage) make

clean:
	rm -rf **/*~ **/*-eps-converted-to.pdf *-eps-converted-to.pdf $(addprefix $(NAME),.aux .dvi .log .nav .out .pdf .snm .toc .vrb -pics.pdf -autopp.vrb -autopp.log -autopp.run.xml -blx.bib .bcf .run.xml .bbl .blg .html .url .fdb_latexmk fls .fdb_latexmk .fls) auto
